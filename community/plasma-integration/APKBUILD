# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-integration
pkgver=5.25.2
pkgrel=0
pkgdesc="Qt Platform Theme integration plugins for the Plasma workspaces"
# armhf blocked by qt5-qtdeclarative
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://kde.org/plasma-desktop/"
license="(LGPL-2.1-only OR LGPL-3.0-only) AND LGPL-2.1-only AND ((LGPL-2.1-only WITH Nokia-Qt-exception-1.1) OR (GPL-3.0-only WITH Nokia-Qt-exception-1.1))"
depends="
	font-noto
	qqc2-desktop-style
	ttf-hack
	"
makedepends="
	breeze-dev
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knotifications-dev
	kwayland-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	libxcursor-dev
	qt5-qtbase-dev
	qt5-qtquickcontrols2-dev
	qt5-qtx11extras-dev
	samurai
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-integration-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # Broken

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_DISABLE_FIND_PACKAGE_FontNotoSans=true \
		-DCMAKE_DISABLE_FIND_PACKAGE_FontHack=true
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
2f51a0cc2e64475eb0e506c05af89e5299f5fa5ecb63d825f79a445db0d0ca7101c5aac0c1fb3737045fc60d3599bb8600002ca06963d79ad30a113679dd5f01  plasma-integration-5.25.2.tar.xz
"
